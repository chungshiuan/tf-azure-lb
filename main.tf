module "lb" {
  source  = "github.com/Azure/terraform-azurerm-loadbalancer"
  location=var.location
  resource_group_name = var.resource_group_name
  prefix = var.prefix
  remote_port = var.remote_port
  lb_port = var.lb_port
  lb_probe_unhealthy_threshold = var.lb_probe_unhealthy_threshold
  lb_probe_interval = var.lb_probe_interval
  frontend_name = var.frontend_name
  allocation_method = var.allocation_method
  tags = var.tags
  frontend_subnet_id = var.frontend_subnet_id
  frontend_private_ip_address = var.frontend_private_ip_address
  frontend_private_ip_address_allocation = var.frontend_private_ip_address_allocation
  lb_sku = var.lb_sku
  lb_probe = var.lb_probe
  pip_sku = var.pip_sku
  name = var.name
  pip_name = var.pip_name
}